const { showError, showSuccess } = require("./postResponses");
const formidable =  require('formidable')
const querystring = require("querystring");
const sharp = require("sharp");
const ExcelJS = require("exceljs");
const { promisify } = require('util')

async function handlePostRequests(req, res, parsedUrl) {
  let urlPath = parsedUrl.pathname;

  // console.log("urlPath", urlPath);
  let rootPath = `${req.headers["x-forwarded-proto"]}://${req.headers["x-forwarded-host"]}`;
  // console.log(req.headers);

  urlPath = urlPath.toLowerCase();

  // console.log("handlePostRequests", urlPath);
  try {
    switch (true) {
      case urlPath.startsWith("/generate_report"): {
        let data = "";
        for await (let chunk of req) data += chunk;
        data = JSON.parse(data.toString());
        
        //* секция Проверки
        

        //* endof Проверки

        //* секция Генерация отчета
        let result = {url: '', fileName: ''}
        //* endof Генерация отчета

        return showSuccess(res, null, result);
        break;
      }

      case urlPath.startsWith("/process_image"): {
        let data = "";
        for await (let chunk of req) data += chunk;
        data = JSON.parse(data.toString());
        
        //* секция Проверки
        

        //* endof Проверки

        //* секция Обработка изображения
        
        //* endof Обработка изображения

        return showSuccess(res, null, result);
        break;
      }
      default: {
        console.log("Incorrect request path", urlPath);
        return showError(res, "Incorrect request path");
        break;
      }
    }
  } catch (e) {
    let errorParams = { message: e.message, stackStart: e.stack.split("\n", 3) };
    console.log(errorParams);
    logServerError("handle POST error", errorParams);
    return showError(res, e.message, errorParams);
  }
}

module.exports = handlePostRequests;
